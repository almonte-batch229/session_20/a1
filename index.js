let numberVar = Number(prompt("Please provide a number:"))

for(let currentNumber = numberVar; currentNumber >= 0; currentNumber--) {

	if(currentNumber <= 50) {
		console.log("The current value is at 50. Terminating the loop.")
		break
	}

	if(currentNumber % 10 === 0) {
		console.log("The number is divisible by 10. Skipping the number.")
		continue
	}

	if(currentNumber % 5 === 0) {
		console.log(currentNumber)
	}
}

let longestWord = "supercalifragilisticexpialidocious"

console.log(longestWord)

let consonants = []

for(let letter = 0; letter < longestWord.length; letter++) {

	if(longestWord[letter].toLowerCase() == "a" || longestWord[letter].toLowerCase() == "e" || longestWord[letter].toLowerCase() == "i" || longestWord[letter].toLowerCase() == "o" || longestWord[letter].toLowerCase() == "u") {
		continue
	}
	else {
		consonants += longestWord[letter]
	}
}

console.log(consonants)